---
title: '<br><br><br><small>**Synaptic plasticity and functional connectivity in cortical brain networks**</small>'
pagetitle: "Memory traces, Vassilis Kehayas"
author: 'Vassilis Kehayas<br>vkehayas@ics.forth.gr<br><br>https://memory-traces.netlify.app/'
date: "9/11/2022"
output:
  revealjs::revealjs_presentation:
    css: custom.css
    reveal_options:
      slideNumber: true
      previewLinks: true
      hash: true
    slide_level: 3
    transition: none
  powerpoint_presentation:
    slide_level: 3
  slidy_presentation:
    css: custom.css
    font-family: Georgia
    toc: yes
    toc_depth: 2
    toc_float: yes
  ioslides_presentation:
    css: custom.css
  beamer_presentation: default
---

###

<section id="zebrafish"
         data-background-video="img/mmc4_edit.mkv" 
         data-background-video-loop 
         data-background-video-muted
         data-background-size="cover"
         <h3 style="font-weight:bold">Neural representation of a moving object in the optical tectum of a zebrafish larva</h3>
</section>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<small id="zebrafish-ref" style="text-align: right">
<a style="color: #b0b0c7">https://doi.org/10.1016/j.cub.2012.12.040</a>
</small>

### **Neurons communicate using electrical impulses and neurotransmission**

<img src="img/neurons_synapse_structure.png" style="width:65%; height:65%; text-align:center;">
<small>
OpenStax College, Biology ([CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)).
</small>

### 
<section id="kasthuri"
         data-background-video="img/kasthuri_edit.mkv" 
         data-background-video-loop 
         data-background-video-muted
         data-background-size="cover"
         <h3 style="font-weight:bold">Fine anatomy of synaptic connectivity</h3>
</section>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<small id="kasthuri-ref" style="text-align: right">
<a style="color: #b0b0c7">https://doi.org/10.1016/j.cell.2015.06.054</a>
</small>

### **Lasting memory traces:<br>The reverberation hypothesis**

<video data-autoplay loop="true" src="img/ferezou4.mp4" style="width:50%"></video>

<small>
Ferezou et al., 2007. Neuron 56, 907–923.  
https://doi.org/10.1016/j.neuron.2007.10.007
</small>

### **Lasting memory traces:<br>The dual-trace hypothesis**

<img src="img/hebb.png" style="width:35%"/>

<small>
Hebb, D.O., 1949 (2002).  
The Organization of Behavior: A Neuropsychological Theory. Routledge.  
https://doi.org/10.4324/9781410612403
</small>

### **Lasting memory traces:<br>The dual-trace hypothesis**

<br>
<small>

>When an axon of cell A is near enough to excite a cell B and repeatedly or persistently takes part in firing it, some growth process or metabolic change takes place in one or both cells such that A's efficiency, as one of the cells firing B, is increased.  

<br>
<br>
<br>
<br>
<br>
Hebb, D.O., 1949 (2002).  
The Organization of Behavior: A Neuropsychological Theory. Routledge.  
https://doi.org/10.4324/9781410612403
</small>

### **Long-term potentiation (LTP) as a synaptic model of memory**

<img src="img/bliss_1993.png" style="height:470px; text-align:center;">  
<small>
Bliss & Collingridge, 1993.  
Nature, 361(6407).  
https://doi.org/10.1038/361031a0
</small>

### **Lasting memory traces:<br>The dual-trace hypothesis**

<small>

>When an axon of cell A is near enough to excite a cell B and repeatedly or persistently takes part in firing it, some growth process or metabolic change takes place in one or both cells such that A's efficiency, as one of the cells firing B, is increased.  
**The most obvious and I believe much the most probable suggestion concerning the way in which one cell could become more capable of firing another is that synaptic knobs develop and increase the area of contact between the afferent axon and efferent soma.**

</small>
<small>
Hebb, D.O., 1949 (2002).  
The Organization of Behavior: A Neuropsychological Theory. Routledge.  
https://doi.org/10.4324/9781410612403
</small>

### **Individual spines show LTP**

<img src="img/harvey_2007.png" style="height:550px; text-align:center;">  
<small>
Harvey & Svoboda, 2007.  
Nature; 450(7173):1195-1200.
</small>

### **New spines are formed after tetanic stimulation**

<br>

<img src="img/maletic_1999.png" style="text-align:center;">  

<br>

<small>
Maletic-Savatic, Malinow, and Svoboda.  
Science 283, no. 5409 (March 19, 1999): 1923–27.  
https://doi.org/10.1126/science.283.5409.1923.
</small>

### **Summary (I)**

<br>

<small>
<div style="text-align:left; width:550px">
The predictions of the dual-trace theory are compatible with experimental evidence on functional and structural synaptic plasticity  

- Long-term potentiation appears as a likely mechanism underlying memory formation  
- Long-term potentiation has direct effects on cellular structure through the formation and enlargement of dendritic spines
</div>
</small>

### **LTP can be induced *in vivo* after rhythmic whisker stimulation (RWS)**

<br>

<img src="img/gambino_2014_rwsltp.png" style="width:700px; text-align:center;">  


<small>
Gambino, Pagès, Kehayas, et al.,  
Nature 515, no. 7525 (November 2014): 116–19.  
https://doi.org/10.1038/nature13664.
</small>

### **RWS-LTP depends on the occurrence of NMDA-dependent plateau potentials**

<br>

<img src="img/gambino_2014_rwsplateau.png" style="width:700px; text-align:center;">  

<small>
Gambino, Pagès, Kehayas, et al.,  
Nature 515, no. 7525 (November 2014): 116–19.  
https://doi.org/10.1038/nature13664.
</small>

### **NMDA spikes cause persistent potentiation of task-related dendritic spines**

<br>

<img src="img/cichon_2015.png" style="width:750px; text-align:center;">  


<small>
Cichon & Gan, 2015.  
Nature 520, 7546: 180–85.  
https://doi.org/10.1038/nature14251.
</small>

### **Anatomy of the whisker-to-barrel system**

<img src="img/whisker_barrel.png" style="height:550px;text-align:center;">  

### **Anatomy of the whisker-to-barrel system**

<img src="img/wimmer_2010.png" style="text-align:center;">  

<small>
Wimmer, Bruno, de Kock, Kuner, and Sakmann.  
Cerebral Cortex 20, no. 10 (October 1, 2010): 2265–76.  
https://doi.org/10.1093/cercor/bhq068.
</small>

### **Anatomy of the whisker-to-barrel system**

<img src="img/barrel_intra.png" style="height:630px;text-align:center;">  

### ***In vivo* POm activation induces NMDA-dependent plateau potentials in L2/3 cells**

<img src="img/pom_chr.png" style="width:700px; text-align:center;">  

<small>
Gambino, Pagès, Kehayas, et al.,  
Nature 515, no. 7525 (November 2014): 116–19.  
https://doi.org/10.1038/nature13664.
</small>

### **POm-dependent plateau potentials are necessary for the induction of RWS-LTP**

<img src="img/pom_muscimol.png" style="height:450px; text-align:center;">  

<small>
Gambino, Pagès, Kehayas, et al.,  
Nature 515, no. 7525 (November 2014): 116–19.  
https://doi.org/10.1038/nature13664.
</small>

### **Summary (II)**

<br>

<small>
<div style="text-align:left; width:675px">
- Rhythmic whisker stimulation (RWS) induces long-term potentiation (LTP) *in vivo*  
- RWS-LTP depends on the occurrence of NMDA-dependent plateau potentials  
- POm-dependent plateau potentials are necessary for the induction of RWS-LTP
</div>
</small>

### **New spines are stabilized after whisker trimming**

<img src="img/holtmaat_2009.png" style="height:440px; text-align:center;">  

<small>
Holtmaat & Svoboda (2009).  
Nature Reviews Neuroscience, 10(9), 647–658.  
https://doi.org/10.1038/nrn2699
</small>

### **Selective shrinkage of newly activated spines**

<img src="img/hayashi-takagi_2015_setup.png" style="height:450px; text-align:center;">  

<small>
Hayashi-Takagi et al. (2015).  
Nature, 525(7569)  
https://doi.org/10.1038/nature15257
</small>

### **Erasure of acquired learning by the photoactivation of spines labelled with AS-PaRac1**

<img src="img/hayashi-takagi_2015_pa.png" style="height:400px; text-align:center;">  
<small>
Hayashi-Takagi et al. (2015).  
Nature, 525(7569)  
https://doi.org/10.1038/nature15257
</small>

### **Summary (III)**

<br>

<small>
<div style="text-align:left; width:675px">
- Experience stabilizes newly created spines  
- Newly activated spines are necessary for the retention of learned behavioural responses
</div>
</small>

### **During development there is a shift from a predominance of shaft-synapses to synapses on spines**

<br>
<img src="img/shaft_spine_cartoon_timeline.png" style="width:600px; text-align:center;">  

### **The Miller-Peters hypothesis for the emergence of spines**

<img src="img/miller_shaft.png" style="height:400px; text-align:center;">  
<small>
Miller and Peters (1981)  
Journal of Comparative Neurology 203, no. 4: 555–73.  
https://doi.org/10.1002/cne.902030402.
</small>

### **Spine growth precedes synapse formation**

<img src="img/knott_2006.png" style="height:400px; text-align:center;">  
<small>
Knott et al. (2006)  
Nature Neuroscience 9, no. 9: 1117–24.  
https://doi.org/10.1038/nn1747.
</small>


### ***In vivo* imaging of synapses on dendritic shafts and spines in the barrel cortex of adult mice**

<img src="img/cane_imaging.png" style="height:450px; text-align:center;">  
<small>
Cane et al. (2014).  
Journal of Neuroscience, 34(6), 2075–2086.  
https://doi.org/10.1523/JNEUROSCI.3353-13.2014
</small>

### **Shaft synapses are found further away from always-present spines than expected assumming a uniform distribution of positions**

<img src="img/ap_shaft_sim.png" style="width:400px; text-align:center;">  

<small>
Kehayas et al., *In preparation*
</small>

### **Shaft-synapse survival strongly depends on the existence of a synapse on its closest spine neighbour**

<img src="img/shaft_survival_glmm_covered.svg" style="width:415px; text-align:center;">  

<small>
Kehayas et al., *In preparation*
</small>

### **Shaft-synapse survival strongly depends on the existence of a synapse on its closest spine neighbour**

<img src="img/shaft_survival_glmm.svg" style="width:415px; text-align:center;">  

<small>
Kehayas et al., *In preparation*
</small>

### **Heterosynaptic plasticity between spines on the same dendritic segment**

<img src="img/oh_cartoon.png" style="width:415px; text-align:center;">  

<small>
Oh, Parajuli, & Zito, K. (2015).  
Cell Reports, 10(2), 162–169.  
https://doi.org/10.1016/j.celrep.2014.12.016
</small>

### **Summary (IV)**

<br>

<small>
<div style="text-align:left; width:675px">
- Heterosynaptic plasticity between neighbouring synapses on the same dendrite may lead to potentiation or shrinkage
</div>
</small>

### **Hebbian learning leads to the formation of functionally linked cell assemblies**

<br>
<img src="img/gerstner_assemblies.png" style="width:415px; text-align:center;">  

<small>
Gerstner, Kistler, Naud and Paninski (2014)  
Neuronal Dynamics: From single neurons to networks and models of cognition  
Cambridge University Press
</small>


### **Spontaneous firing of action potentials is, on average, uncorrelated between pairs of neurons**

<img src="img/renart.png" style="width:800px; text-align:center;">  

<small>
Renart, A., J. de la Rocha, P. Bartho, L. et al.  
Science 327, no. 5965 (January 29, 2010): 587–90.  
https://doi.org/10.1126/science.1179850
</small>

### **Mesoscopic *in vivo* two-photon imaging of calcium activity in awake mice**

<img src="img/mesoscope.png" style="text-align:center;">  

<small>
Kehayas et al., *In preparation*
</small>

### **The small-world architecture provides efficiency for information transmission**

<img src="img/watts_strogatz.png" style="width:45%; height:45%; text-align:center;"/>

<small>
Watts, Duncan J., and Steven H. Strogatz.  
Nature 393, no. 6684 (June 1, 1998): 440–42.  
https://doi.org/10.1038/30918
</small>
<div style="text-align:left; width:675px">

### **Observed networks follow a “small-world”-like architecture**

<img src="img/small_world.png" style="height:500px; text-align:center;">  

<small>
Kehayas et al., *In preparation*
</small>

### **Summary(V)**

<br>

<small>
<div style="text-align:left; width:675px">
- Cortical networks display non-random connectivity  
- The "small-world" network architecture is conducive to efficient and robust information transmission
</div>
</small>

### **Changes in functional connectivity between brain areas during a verbal learning task in humans**


<img src="img/fmri.png" style="height:500px; text-align:center;">  

<small>
Kehayas et al., *In preparation*
</small>

### **Conclusion**

<br>

<small>
<div style="text-align:left; width:675px">
- Cortical networks are built for connectivity  
- Cortical networks change their function and structure based on shared activity patterns  
- Changes of neuronal function and structure are necessary for certain types of learning  
- The study of brain networks has the potential to predict behavioural learning
</div>
</small>

### 

<br>
<br>
<br>
<br>
<br>
<h1>Thank you!</h1>
